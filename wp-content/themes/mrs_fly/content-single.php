<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>


<?php
					$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'ljd_naujiena');



			?>

            			<article class="single_news row">
                        	<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><img src="<?php echo $thumbnail[0]; ?>"  alt="<?php the_title_attribute(); ?>"></a>
                        	<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><h3><?php the_title(); ?></h3></a>
                            <div class="time_stamp"><?php the_time( 'o F d' ); ?>d.</div>
                            <p><?php the_content(); ?></p>

                        </article><!--single_news-->
