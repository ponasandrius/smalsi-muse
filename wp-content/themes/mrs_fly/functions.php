<?php

load_theme_textdomain( 'cervus', TEMPLATEPATH.'/lang' );
//$options = get_option( 'cc4u_theme_options' );
define('ELN', get_template_directory_uri().'/');

// Adds all custom functions
require_once(get_template_directory().'/includes/functions/custom_functions.php');

// Theme support options
require_once(get_template_directory().'/includes/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/includes/functions/cleanup.php');

// Register scripts and stylesheets
require_once(get_template_directory().'/includes/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory().'/includes/functions/menu.php');
require_once(get_template_directory().'/includes/functions/menu-walkers.php');

// Register sidebars/widget areas
require_once(get_template_directory().'/includes/functions/sidebar.php');

// Makes WordPress comments suck less
require_once(get_template_directory().'/includes/functions/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/includes/functions/page-navi.php');

// Adds support for multiple languages
require_once(get_template_directory().'/includes/translation/translation.php');

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/includes/functions/editor-styles.php');

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/includes/functions/related-posts.php');

// Use this as a template for custom post types
// require_once(get_template_directory().'/includes/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/includes/functions/login.php');

// Customize the WordPress admin
// require_once(get_template_directory().'/includes/functions/admin.php');

// Customize the WordPress theme options
require_once(get_template_directory().'/includes/functions/theme_admin.php');


// Add custom post type for main page information

//require_once(get_template_directory().'/includes/custom-types/products.php');
require_once(get_template_directory().'/includes/custom-types/slides.php');
require_once(get_template_directory().'/includes/custom-types/trips.php');
require_once(get_template_directory().'/includes/custom-types/tips.php');
require_once(get_template_directory().'/includes/custom-types/top.php');



//require_once(get_template_directory().'/includes/functions/encrypt.php');

// Include ajax things
require_once(get_template_directory().'/includes/ajax/ajax.php');

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');