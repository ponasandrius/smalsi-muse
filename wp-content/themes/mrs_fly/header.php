<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="icon"
      type="image/png"
      href="<?php echo get_template_directory_uri();?>/public/images/favicon.png" />

    <?php wp_head(); ?>
</head>
<?php
$body_class = 'non-home';
    if(is_front_page() || is_home() ){
        $body_class = '';
    }
?>
<body <?php body_class($body_class); ?>>

<header class="main-header">
    <div class="main-header__wrapper">
        <nav class="navigation-main">
            <a href="#" class="logo-main"><img src="<?php echo get_template_directory_uri();?>/public/images/smalsi-muse-logotipas.png" alt="Smalsi musė logotipas"></a>
            <ul class="navigation-main__wrapper">
                <li class="menu-item menu-item--filter">
                    <a href="#" id="js-filters-toggle"><?php _e('Filtras', 'tandem'); ?><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    
                </li>
                <?php joints_top_nav(); ?>
                <li class="menu-item menu-item--search">
                    <form action="" class="search-field">
                        <input type="text" class="search-field__input" placeholder="<?php _e('Paieška', 'tandem'); ?>">
                        <button type="submit" class="search-field__submit">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </form>
                </li>
            </ul>            
        </nav>

        <button type="button" class="navigation-main__toggle">
            <span class="navigation-main__toggle__line"></span>
            <span class="navigation-main__toggle__line"></span>
            <span class="navigation-main__toggle__line"></span>
        </button>
        <div class="filters-menu" id="js-filters-menu">
            <div class="filters-menu__inner">
                <div class="filters-menu__filters" id="js-filters-menu__filters">
                    <form class="filters-menu__filters-form" id="js-filters-menu__filters-form">
                    
                    </form>
                </div>
                <div class="filters-menu__line">&nbsp;</div>
                <div class="filters-menu__countries" id="js-filters-menu__countries">
                     <section class="filters-menu__section">
                            <h3><?php _e('Pasirinkite šalį:', 'tandem'); ?></h3>
                            <form class="filters-menu__countries-form" id="js-filters-menu__countries-form"></form>
                    </section>
                    <div class="filters-menu__button-holder">
                        <button type="button" class="btn" id="js-display-filtered-trips"><?php _e('Ieškoti kelionių', 'tandem'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
