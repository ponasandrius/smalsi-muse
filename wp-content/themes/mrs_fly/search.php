<?php get_header(); ?>
<div></div>
        <div id="main_content" class="row">
        	<div id="content" class="columns large-9">


 <section id="news" class="row">
                                <div class="columns large-12" >
<h1 class="page-title"><?php printf( __( 'Paieškos rezultatai: %s'), '<span>' . get_search_query() . '</span>' ); ?></h1>
        <?php

        	if ( have_posts() ) { $count = 0;
        		while ( have_posts() ) { the_post(); $count++;
					?>
                      <?php get_template_part( 'content', get_post_format() ); ?>


            	<?php
				} // End WHILE Loop
			} else {
		?>
			<article <?php post_class(); ?>>
            	<p>Nieko neradome.</p>
            </article><!-- /.post -->
        <?php } // End IF Statement ?>


            </div>
            </section>


            </div><!--content-->
            <div id="sidebar" class="columns large-3">
            	<?php get_sidebar(); ?>
            </div><!--sidebar-->


        </div>
<?php get_footer(); ?>
