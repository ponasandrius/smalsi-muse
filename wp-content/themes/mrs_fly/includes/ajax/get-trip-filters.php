<?php

add_action( 'wp_ajax_nopriv_load_trips_filters', 'load_trips_filters' );
add_action( 'wp_ajax_load_trips_filters', 'load_trips_filters' );

function load_trips_filters() {

    // $nonce = $_POST['nonce'];
     header( "Content-Type: application/json" );
/*
     $tokenFactory = new TokenMaker();
     $token = $tokenFactory->createToken();

     // check to see if the submitted nonce matches with the
     // generated nonce we created earlier
     if ( ! wp_verify_nonce( $nonce, 'trips_filters' ) )
        die('{"errors": ["No method selected"]}');*/


     // generate the response
     $taxonomy_objects = get_object_taxonomies( 'tandem_trips', 'objects' );
     $taxonomies = array();

     foreach($taxonomy_objects as $taxonomy) {
        if($taxonomy->name !== 'trip_cat' && $taxonomy->name !== 'trip_country') {
            $newTaxonomy = array();
            $newTaxonomy['name'] = $taxonomy->name;
            $newTaxonomy['label'] = $taxonomy->label;
            $newTaxonomy['terms'] = array();

            $terms = get_terms( array(
                'taxonomy' => $taxonomy->name,
                'hide_empty' => false,
            ) );

            if($terms) {
                $newTaxonomy['terms'] = $terms;
            }

            $taxonomies[] = $newTaxonomy;
        }
     }

     
     // response output
    $response = json_encode($taxonomies);
     echo $response;

     // IMPORTANT: don't forget to "exit"
     exit;
}
