<?php
function ajax_scripts(){
    wp_localize_script( 'my-ajax-request', 'MyAjax', array(
     // URL to wp-admin/admin-ajax.php to process the request
     'ajaxurl'          => admin_url( 'admin-ajax.php' ),

     // generating nonce fields for all ajax requests
     'main_page_prods' => wp_create_nonce( 'main_page_prods')
     )
    );
}

add_action('init', 'ajax_scripts');

require_once(get_template_directory().'/includes/ajax/load-all-prod.php');
require_once(get_template_directory().'/includes/ajax/get-trip-filters.php');
require_once(get_template_directory().'/includes/ajax/get-countries.php');

