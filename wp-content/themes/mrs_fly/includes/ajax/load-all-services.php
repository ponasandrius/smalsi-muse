<?php
// make media query to get all slides
$args = array(
    'posts_per_page' => '100',
    'offset' => '7',
    'post_type'   => 'tandem_services'
  );
  $q = new WP_Query($args);
?>
<?php
$services = array();
            $count_v = 0;
            $count_s = 0;
            $count_h = 0;
           if ($q->have_posts()) {
                    while ($q->have_posts()) {
                        $q->the_post();
                        $or_class = 'square';
                        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-thumbnail' );

                        $thumb = $thumb[0];
                        $or = get_field('thumb_orie');
                        if($or == 1){
                            $count_v++;
                            $or_class = 'vert_col';
                            $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'serv_vert');
                            if(isset($thumbnail[1])){
                                $thumb = $thumbnail[0];
                            }
                        }else
                        if($or == 2){
                            $count_h++;
                            $or_class = 'hor_col';
                            $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'serv_hor');
                            if(isset($thumbnail[1])){
                                $thumb = $thumbnail[0];
                            }
                        }else{
                            $count_s++;
                        }

                        $service = array();
                        $service['title'] = get_the_title();
                        $service['class'] = $or_class;
                        $service['thumb'] = $thumb;
                        $service['permalink'] = get_permalink();

                        $services[] = $service;
                    }

                    echo json_encode($services);
                 }else{
               echo '{"errors": ["'.__('Daugiau paslaugų nėra','tandem').'"]}';
           }

             wp_reset_query();
            ?>
