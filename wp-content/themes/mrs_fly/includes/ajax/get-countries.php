<?php

add_action( 'wp_ajax_nopriv_load_countries', 'load_countries' );
add_action( 'wp_ajax_load_countries', 'load_countries' );

function load_countries() {
    header( "Content-Type: application/json" );
    
    if((isset($_GET) && count($_GET) !== 0) || !isset($_POST)) {
        header("HTTP/1.1 500 Internal Server Error");
        die('{"errors": ["Wrong post type."]}');
    }

    // $nonce = $_POST['nonce'];
     
/*
     $tokenFactory = new TokenMaker();
     $token = $tokenFactory->createToken();

     // check to see if the submitted nonce matches with the
     // generated nonce we created earlier
     if ( ! wp_verify_nonce( $nonce, 'trips_filters' ) )
        die('{"errors": ["No method selected"]}');*/


     // generate the response

     if(!isset($_POST['filters'])) {
         die('{"status": "No countries", "countries" : []}');
     }

     $query_filters = array('relation' => 'AND');

     foreach($_POST['filters'] as $key => $taxonomy) {
         
         $terms = array();

         foreach($taxonomy as $term) {
            $terms[] = $term;
         }

         $taxonomy = array(
             'taxonomy' => str_replace('filter-', '',$key),
             'field' => 'term_id',
             'terms' => $terms,
             'operator' => 'IN'
         );

        $query_filters[] = $taxonomy;
     }

     //die(json_encode($query_filters));

     $query = new WP_Query( array(
            'post_type' => 'tandem_trips',          // name of post type.
            'tax_query' => $query_filters,
            'posts_per_page' => '-1'
        ) 
    );

    $countries = array();

    if ( $query->have_posts() ) {
        
        while ( $query->have_posts() ) {
            $query->the_post();
            $term_list = wp_get_object_terms($query->post->ID, 'trip_country');

            foreach($term_list as $the_term) {
                if(!isset($countries[$the_term->slug])) {
                    $countries[$the_term->slug] = array(
                        'id' => $the_term->term_id,
                        'name' => $the_term->name,
                        'slug' => $the_term->slug
                    );
                }
            }
            
        }
        /* Restore original Post Data */
        wp_reset_postdata();
    }

    $response = array(
        'status' => 'success',
        'countries' => $countries
    );
     
     // response output
    $final_response = json_encode($response);
    echo $final_response;

     // IMPORTANT: don't forget to "exit"
     exit;
}
