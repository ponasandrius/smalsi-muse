<?php

add_action( 'wp_ajax_nopriv_Load-all-cats', 'myajax_submit' );
add_action( 'wp_ajax_Load-all-cats', 'myajax_submit' );

function myajax_submit() {

     $nonce = $_POST['main_page_prods'];
     header( "Content-Type: application/json" );
     // check to see if the submitted nonce matches with the
     // generated nonce we created earlier
     if ( ! wp_verify_nonce( $nonce, 'main_page_prods' ) )
        die('{"errors": ["No method selected"]}');


     // generate the response
     $response = array();
     $categories = get_terms( 'prod_cat', 'hide_empty=0' );
            $cats = array();
            $count = 0;
            $count2 = 0;
            //echo json_encode($categories);
            foreach($categories as $cat){
                $cat_link = get_term_link( $cat );
                if(!$cat->parent && $cat->parent < 1){
                    if($count > 3){
                        $cats[$count2]['url'] = $cat_link;
                        $cats[$count2]['title'] = $cat->name;
                        $cats[$count2++]['img'] = z_taxonomy_image_url($cat->term_id);
                    }
                    $count++;
                }

            }// end foreach
      if(count($cats) > 0){
          $response['cats'] = $cats;
      }else{
         $response['errors'] = __('Kategorijų nėra','tandem');
      }

     // response output
    $response = json_encode($response);
     echo $response;

     // IMPORTANT: don't forget to "exit"
     exit;
}
