<?php

// let's create the function for the custom type
function tandem_slides() {
	// creating (registering) the custom type
	register_post_type( 'tandem_slides', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Skaidrės', 'tandem'), /* This is the Title of the Group */
			'singular_name' => __('Skaidrė', 'tandem'), /* This is the individual type */
			'all_items' => __('Visos skaidrės', 'tandem'), /* the all items menu item */
			'add_new' => __('Pridėti naują', 'tandem'), /* The add new menu item */
			'add_new_item' => __('Pridėti naują skaidrę', 'tandem'), /* Add New Display Title */
			'edit' => __( 'Redaguoti', 'tandem' ), /* Edit Dialog */
			'edit_item' => __('Radaguoti naują skaidrę', 'tandem'), /* Edit Display Title */
			'new_item' => __('Nauja skaidrė', 'tandem'), /* New Display Title */
			'view_item' => __('Rodyti skaidrę', 'tandem'), /* View Display Title */
			'search_items' => __('Ieškoti skaidrės', 'tandem'), /* Search Custom Type Title */
			'not_found' =>  __('Nieko nerasta.', 'tandem'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Šiukšlinėje nieko nerasta', 'tandem'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Skaidrės rodomos pagrindiniame puslapyje', 'jointswp' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => false,
			'menu_position' => 60, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-images-alt2', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'info_blokas', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title')
	 	) /* end of options */
	); /* end of register post type */



}
// adding the function to the Wordpress init
add_action( 'init', 'tandem_slides');

add_filter("manage_edit-tandem_slides_columns", "tandem_slides_edit_columns");

function tandem_slides_edit_columns($columns){
   $columns = array(
                    "cb" => '<input type="checkbox" />',
                    "photo" => __("Skaidrė"),
                    "title" => __("Tekstas")
                   );

   return $columns;
}

add_action("manage_tandem_slides_posts_custom_column",  "slides_custom_columns");

function slides_custom_columns($column){
  global $post;
  switch ($column){
                 case "photo":
                     echo '<img src="'.get_field('slide_img').'" style="width:150px;">';
                 break;
                 case "orientation":
                    $or = get_field('thumb_orie');
                        if($or == 1){
                            echo 'Vertikalus';
                        }else
                        if($or == 2){
                            echo 'Horizontalus';
                        }else{
                            echo 'Kvadratas';
                        }
                 break;

   }
}
