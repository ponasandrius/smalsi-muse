<?php

// let's create the function for the custom type
function tandem_tips_init() {

	register_post_type( 'tandem_tips', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Patarimai', 'tandem'), /* This is the Title of the Group */
			'singular_name' => __('Patarimas', 'tandem'), /* This is the individual type */
			'all_items' => __('Visi patarimai', 'tandem'), /* the all items menu item */
			'add_new' => __('Pridėti naują', 'tandem'), /* The add new menu item */
			'add_new_item' => __('Pridėti naują', 'tandem'), /* Add New Display Title */
			'edit' => __( 'Redaguoti', 'tandem' ), /* Edit Dialog */
			'edit_item' => __('Radaguoti', 'tandem'), /* Edit Display Title */
			'new_item' => __('Naujas patarimas', 'tandem'), /* New Display Title */
			'view_item' => __('Rodyti patarimą', 'tandem'), /* View Display Title */
			'search_items' => __('Ieškoti patarimo', 'tandem'), /* Search Custom Type Title */
			'not_found' =>  __('Nieko nerasta.', 'tandem'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Šiukšlinėje nieko nerasta', 'tandem'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Patarimai', 'tandem' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 20, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-megaphone', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'patarimas', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title','editor','thumbnail','excerpt')
	 	) /* end of options */
	); /* end of register post type */



}
// adding the function to the Wordpress init
add_action( 'init', 'tandem_tips_init');

