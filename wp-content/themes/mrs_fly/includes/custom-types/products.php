<?php

// let's create the function for the custom type
function tandem_veiklos() {
	// creating (registering) the custom type
    register_taxonomy_for_object_type('prod_cat', 'tandem_products');
	register_post_type( 'tandem_products', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Produktai', 'tandem'), /* This is the Title of the Group */
			'singular_name' => __('Produktas', 'tandem'), /* This is the individual type */
			'all_items' => __('Visi produktai', 'tandem'), /* the all items menu item */
			'add_new' => __('Pridėti naują', 'tandem'), /* The add new menu item */
			'add_new_item' => __('Pridėti naują', 'tandem'), /* Add New Display Title */
			'edit' => __( 'Redaguoti', 'tandem' ), /* Edit Dialog */
			'edit_item' => __('Radaguoti', 'tandem'), /* Edit Display Title */
			'new_item' => __('Naujas produktas', 'tandem'), /* New Display Title */
			'view_item' => __('Rodyti produktą', 'tandem'), /* View Display Title */
			'search_items' => __('Ieškoti produkto', 'tandem'), /* Search Custom Type Title */
			'not_found' =>  __('Nieko nerasta.', 'tandem'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Šiukšlinėje nieko nerasta', 'tandem'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Produktai', 'tandem' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 20, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-cart', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'produktas', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title','editor','thumbnail','excerpt')
	 	) /* end of options */
	); /* end of register post type */



}
// adding the function to the Wordpress init
add_action( 'init', 'tandem_veiklos');




register_taxonomy( 'prod_cat',
    array('tandem_products'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    array('hierarchical' => true,     /* if this is true, it acts like categories */
        'labels' => array(
            'name' => __( 'Kategorijos', 'tandem' ), /* name of the custom taxonomy */
            'singular_name' => __( 'Kategorija', 'tandem' ), /* single taxonomy name */
            'search_items' =>  __( 'Ieškoti', 'tandem' ), /* search title for taxomony */
            'all_items' => __( 'Kategorija', 'tandem' ), /* all title for taxonomies */
            'parent_item' => __( 'Tėvinė kategorija', 'tandem' ), /* parent title for taxonomy */
            'parent_item_colon' => __( 'Tėvinė kategorija:', 'tandem' ), /* parent taxonomy title */
            'edit_item' => __( 'Redaguoti', 'tandem' ), /* edit custom taxonomy title */
            'update_item' => __( 'Atnaujinti', 'tandem' ), /* update title for taxonomy */
            'add_new_item' => __( 'Pridėti', 'tandem' ), /* add new title for taxonomy */
            'new_item_name' => __( 'Kategorija', 'tandem' ) /* name title for taxonomy */
        ),
        'show_admin_column' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'kategorija' ),
    )
);





add_filter("manage_edit-tandem_veiklos_columns", "garsas1_edit_columns");

function garsas1_edit_columns($columns){
   $columns = array(
                    "cb" => '<input type="checkbox" />',
                    "photo" => __("Pav"),
                    "title" => __("Veikla"),
                    "orientation" => __("Orientacija"),
                    "language" => __("Kalba"),
                    "date" => __("Data")
                   );

   return $columns;
}

add_action("manage_tandem_veiklos_posts_custom_column",  "garsas1_custom_columns");

function garsas1_custom_columns($column){
  global $post;
  switch ($column){
                 case "photo":
                     if(has_post_thumbnail()) the_post_thumbnail(array(50,50));
                 break;
                 case "orientation":
                    $or = get_field('thumb_orie');
                        if($or == 1){
                            echo 'Vertikalus';
                        }else
                        if($or == 2){
                            echo 'Horizontalus';
                        }else{
                            echo 'Kvadratas';
                        }
                 break;

   }
}
