<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    // Load What-Input files in footer
    //wp_enqueue_script( 'what-input', get_template_directory_uri() . '/bower_components/what-input/what-input.min.js', array(), '', true );


    // Adding scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/public/bundle.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/public/plugins/slick.min.js', array( 'jquery' ), '', true );
    wp_localize_script( 'site-js', 'MyAjax', array(
     // URL to wp-admin/admin-ajax.php to process the request
     'ajaxurl'          => admin_url( 'admin-ajax.php' ),
     'tripsArchiveURL' => get_permalink(get_field('trips_archive', 'option')),
     // generating nonce fields for all ajax requests
     'main_page_prods' => wp_create_nonce( 'main_page_prods')
     )
    );
    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/public/styles/style.css', array(), '', 'all' );

}
add_action('wp_enqueue_scripts', 'site_scripts', 999);

