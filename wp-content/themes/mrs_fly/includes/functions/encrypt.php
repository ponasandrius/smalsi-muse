<?php

class Encoder {

    private $random = 'as65d4a6s8asd646a5sd468a';

    public function encode($string) {
        return base64_encode($this->random . base64_encode($string));
    }

    public function decode($string) {
        try {
            $decoded = base64_decode(str_replace($this->random, '', base64_decode($string)));
        }catch(Exception $e) {
            return false;
        }
        return $decoded;
    }
}


class TokenMaker {
    public function createToken() {
        $now = time();
        $ip = $_SERVER['REMOTE_ADDR'];
        $encoder = new Encoder();
        return $encoder->encode(
            json_encode([
                'ip'=>$ip,
                'timestamp'=>$now
            ])
        );
    }

    public function isTokenValid($token) {
        $now = time();
        $ip = $_SERVER['REMOTE_ADDR'];
        $encoder = new Encoder();

        $decoded = $encoder->decode($token);

        if(!$decoded) {
            return false;
        }

        try{
            $decodedArray = json_decode($decoded, true);
        } catch(Exception $e) {
            return false;
        }

        if(!isset($decodedArray['ip']) || !isset($decodedArray['timestamp'])) {
            return false;
        }

        if($decodedArray['ip'] !== $ip) {
            return false;
        }

        $timeDifference = $now - $decodedArray['timestamp'];
        $timeDifference = $timeDifference / 60 / 60;

        if($timeDifference > 12) {
            return false;
        } 

        return true;
    }
}
