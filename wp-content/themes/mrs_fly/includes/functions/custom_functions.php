<?php
function disp_img($title){
	$images_url = get_stylesheet_directory_uri().'/assets/images/';
	echo $images_url.$title;
}

function get_part($title){
	get_template_part( 'includes/parts/'.$title );
}


// get image id from its url
function get_attachment_id_from_src ($image_src) {
    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
    $id = $wpdb->get_var($query);
    return $id;
}


function disp_prods_in_cat_list($cat_id){
    $args = array(
        'post_type'       => 'tandem_products',
        'posts_per_page' => -1,
        // select products by category id
        'tax_query' => array(
            array(
                'taxonomy' => 'prod_cat',
                'field' => 'id',
                'terms' => $cat_id
            )
        )
    );
    $q = new WP_Query($args);
    if ( $q->have_posts() ) {
         $pid = 0;
         if(defined('CURR_PROD')){
            $pid = CURR_PROD;
         }
        echo '<ul>';
        while ( $q->have_posts() ) { $q->the_post();

            ?>
            <li class="<?php echo ($pid==get_the_id()?'active1':''); ?>">
                <a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a>
            </li>
            <?php
        } // end while

        echo '</ul>';
    }// end if
    wp_reset_query();
}




function disp_cats_in_cat_list($cats,$curr_cat){
    foreach($cats as $cat){
        $cat_link = get_term_link( $cat );
        echo '<ul>';
        ?>
        <li >
            <a class="<?php if($curr_cat == $cat->term_id){echo 'active-sbcat';} ?>" href="<?php echo esc_url( $cat_link ); ?>" ><?php echo $cat->name; ?></a>
            <?php
                if($curr_cat == $cat->term_id) disp_prods_in_cat_list($cat->term_id);
            ?>
        </li>
        <?php
        echo '</ul>';
    }
}


// display list of colors
function disp_colors(){
    if( have_rows('spalvos') ): ?>
    <section>
                <h5><?php _e('Spalvos','tandem'); ?></h5>
                <div class="colors">
                    <ul>
                        <?php while( have_rows('spalvos') ): the_row(); ?>

                            <li class="color" style="background:<?php the_sub_field('spalva'); ?>" data-tooltip aria-haspopup="true" class="has-tip" data-disable-hover="false" tabindex="1" title="<?php the_sub_field('spalva_ral'); ?>">



                            </li>

                        <?php endwhile; ?>
                    </ul>
                </div>
            </section>

<?php endif;
}

function disp_colors_p(){
    if( have_rows('spalvos') ){ ?>

        <div class="colors">
            <ul>
                <?php while( have_rows('spalvos') ): the_row(); ?>

                    <li class="color" style="background:<?php the_sub_field('spalva'); ?>" data-tooltip aria-haspopup="true" class="has-tip" data-disable-hover="false" tabindex="1" title="<?php the_sub_field('spalva_ral'); ?>"></li>

                <?php endwhile; ?>
            </ul>
        </div>
<?php }else{
        the_excerpt();
    }
}



function disp_files(){
    if( have_rows('failai') ): ?>
        <?php while( have_rows('failai') ): the_row(); ?>
            <a href="<?php the_sub_field('failas'); ?>" target="_blank" class="spec-btn down"><?php the_sub_field('failo_pavadinimas'); ?></a>
        <?php endwhile; ?>
<?php endif;
}

function disp_first_file(){
    $tras = true;
    if( have_rows('failai') ): ?>
        <?php while( have_rows('failai') && $tras): the_row(); ?>
            <a href="<?php the_sub_field('failas'); ?>" target="_blank" class="download-btn"><?php the_sub_field('failo_pavadinimas'); ?></a>
            <?php $tras = false; ?>
        <?php endwhile; ?>
<?php endif;
}


function disp_gallery(){
    $images = get_field('galerija');

    if( $images ): ?>
        <h3><?php _e('Galerija','tandem'); ?></h3>
            <div class="prod-galery row">
            <?php foreach( $images as $image ): ?>
               <div class="column medium-6 large-4">
                   <?php echo slb_activate('<a href="'.$image['url'].'"><img src="'.$image['sizes']['thumbnail'].'" alt="'.$image['alt'].'"></a>'); ?>
                </div>
            <?php endforeach; ?>
        </div>
<?php endif;
}



function disp_personal_contacts(){
if( have_rows('darbuotoju_kontaktai') ): ?>
    <?php while( have_rows('darbuotoju_kontaktai') ): the_row(); ?>
        <div class="cont-block personal-contact">
            <h6><?php the_sub_field('darbuotojo_vardas'); ?></h6>
            <small><?php the_sub_field('darbuotojo_pareigos'); ?></small>
            <p><?php _e('El. paštas: ','tandem'); ?><?php the_sub_field('darbuotojo_email'); ?></p>
            <p><?php _e('Mob. tel.: ','tandem'); ?><?php the_sub_field('darbuotojo_tel_nr'); ?></p>
        </div>
    <?php endwhile; ?>
<?php endif;
}


function disp_padal_contacts(){
    if( have_rows('padaliniu_kontaktai') ): ?>
        <?php while( have_rows('padaliniu_kontaktai') ): the_row(); ?>
            <h3><?php the_sub_field('padalinio_pavadinimas'); ?></h3>
            <?php if( have_rows('padalinio_kontaktai') ): ?>
                <?php while( have_rows('padalinio_kontaktai') ): the_row(); ?>
                    <div class="cont-block personal-contact">
                        <h6><?php the_sub_field('darbuotojo_vardas'); ?></h6>
                        <small><?php the_sub_field('darbuotojo_pareigos'); ?></small>
                        <p><?php _e('El. paštas: ','tandem'); ?><?php the_sub_field('darbuotojo_email'); ?></p>
                        <p><?php _e('Mob. tel.: ','tandem'); ?><?php the_sub_field('darbuotojo_tel_nr'); ?></p>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>

        <?php endwhile; ?>
    <?php endif;
}

function disp_distributors(){
if( have_rows('platintojai') ): ?>
   <h3 class="column"><?php _e('Platintojai','tandem'); ?></h3>
    <?php while( have_rows('platintojai') ): the_row(); ?>
        <div class="column medium-4">
           <div class="cont-block">
                <h6><?php the_sub_field('platintojo_pavadinimas'); ?></h6>
                <p><?php _e('Adresas: ','tandem'); ?><?php the_sub_field('platintojo_adresas'); ?></p>
                <p><?php _e('El. paštas: ','tandem'); ?><?php the_sub_field('platintojo_email'); ?></p>
                <p><?php _e('Mob. tel.: ','tandem'); ?><?php the_sub_field('platintojo_tel_nr'); ?></p>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif;
}


function displ_cont_form_by_lang(){
    $shortcode = '[contact-form-7 id="80" title="Kontaktų forma 1"]';
    
    if(qtrans_getLanguage() == 'en'){
        $shortcode = '[contact-form-7 id="392" title="Kontaktų forma 1_copy"]';
    }else if(qtrans_getLanguage() == 'RU'){
        $shortcode = '[contact-form-7 id="393" title="Kontaktų forma 1_copy_copy"]';
    }
    echo do_shortcode($shortcode);
}


