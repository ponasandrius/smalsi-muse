<?php

// Adding WP Functions & Theme Support
function joints_theme_support() {

	// Add WP Thumbnail Support
	add_theme_support( 'post-thumbnails' );
    add_post_type_support( 'page', 'excerpt' );
	// Default thumbnail size
	set_post_thumbnail_size(300, 300, true);
    add_image_size( 'medium-img', 500, 500, true );




	// Add RSS Support
	add_theme_support( 'automatic-feed-links' );

	// Add Support for WP Controlled Title Tag
	add_theme_support( 'title-tag' );

	// Add HTML5 Support
	add_theme_support( 'html5',
	         array(
	         	'comment-list',
	         	'comment-form',
	         	'search-form',
	         )
	);

	// Adding post format support
	/* add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	); */

} /* end theme support */



//add_action( 'admin_init', 'add_post_gallery_so_14445904' );
//add_action( 'save_post', 'update_post_gallery_so_14445904', 10, 2 );

/**
 * Add custom Meta Box to Posts post type
 */
function add_post_gallery_so_14445904()
{
    add_meta_box(
        'post_gallery',
        'Galerija',
        'post_gallery_options_so_14445904',
        array('page','post','tandem_services','tandem_veiklos'),
        'normal',
        'core'
    );
}

/**
 * Print the Meta Box content
 */
function post_gallery_options_so_14445904()
{
    global $post;
    $gallery_data = get_post_meta( $post->ID, 'gallery_data', true );

    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'noncename_so_14445904' );
?>

<div id="dynamic_form">

    <div id="field_wrap" class="attachments ui-sortable ui-sortable-disabled">
    <?php
    if ( count($gallery_data) > 0 && isset($gallery_data[0]['image_url']))
    {
        foreach($gallery_data as $imgg)
        {
        ?>

    <a href="#" class="attachment" onClick="remove_field(this); return false;"><img src="<?php echo $imgg['image_thumb']; ?>"  /> <input type="hidden" value="<?php echo $imgg['image_url']; ?>" name="gallery[image_url][]"> <input type="hidden" value="<?php echo $imgg['image_id']; ?>" name="gallery[image_id][]"><input type="hidden" value="<?php echo $imgg['image_name']; ?>" name="gallery[image_name][]"><input type="hidden" value="<?php echo $imgg['image_thumb']; ?>" name="gallery[image_thumb][]"><input type="hidden" value="<?php echo $imgg['image_small']; ?>" name="gallery[image_small][]"></a>

        <?php
        } // endif
    } // endforeach
    ?>
    </div>
    <div style="clear:both;"></div>

    <a href="#" id="add_btnn" class="button button-primary button-large" onClick="addNewImage(); return false;">
    + Prideti foto
    </a>

  <script type="text/javascript">
    function addNewImage(){

      var fileFrame = wp.media.frames.file_frame = wp.media({
              multiple: true
          });
          fileFrame.on('select', function() {
              var url = fileFrame.state().get('selection').first().toJSON();
              //inputField.val(url.url);
              console.log(url);
              var url2 = url.sizes.thumbnail.url;
              jQuery('#field_wrap').append('<a href="#" class="attachment" onClick="remove_field(this); return false;"><img src="'+url2+'" /> <input type="hidden" value="'+url.id+'" name="gallery[image_id][]"><input type="hidden" value="'+url.url+'" name="gallery[image_url][]"><input type="hidden" value="'+url.title+'" name="gallery[image_name][]"><input type="hidden" value="'+url2+'" name="gallery[image_thumb][]"><input type="hidden" value="'+url.sizes.gal_img.url+'" name="gallery[image_small][]"></a>');
          });
          fileFrame.open();
    }

    function remove_field(obj) {
          jQuery(obj).remove();
      }
  </script>

</div>

  <?php
}

/**
 * Save post action, process fields
 */
function update_post_gallery_so_14445904( $post_id, $post_object )
{
    // Doing revision, exit earlier **can be removed**
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;

    // Doing revision, exit earlier
    if ( 'revision' == $post_object->post_type )
        return;

    // Verify authenticity
    if ( !wp_verify_nonce( $_POST['noncename_so_14445904'], plugin_basename( __FILE__ ) ) )
        return;

    // Correct post type


    if ( $_POST['gallery'] )
    {
        // Build array for saving post meta
        $gallery_data = array();
        for ($i = 0; $i < count( $_POST['gallery']['image_id'] ); $i++ )
        {
            if ( '' != $_POST['gallery']['image_id'][ $i ] )
            {
                $gallery_data[$i]['image_id']  = $_POST['gallery']['image_id'][ $i ];
                $gallery_data[$i]['image_url']  = $_POST['gallery']['image_url'][ $i ];
                $gallery_data[$i]['image_name'] = $_POST['gallery']['image_name'][ $i ];
                $gallery_data[$i]['image_thumb'] = $_POST['gallery']['image_thumb'][ $i ];
                $gallery_data[$i]['image_small'] = $_POST['gallery']['image_small'][ $i ];
            }
        }

        if ( $gallery_data )
            update_post_meta( $post_id, 'gallery_data', $gallery_data );
        else
            delete_post_meta( $post_id, 'gallery_data' );
    }
    // Nothing received, all fields are empty, delete option
    else
    {
        delete_post_meta( $post_id, 'gallery_data' );
    }
}
