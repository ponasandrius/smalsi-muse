<?php

/** adding settings page */
if( function_exists('acf_add_options_page') ) {

	$parent = acf_add_options_page(array(
		'page_title' 	=> 'Temos nustatymai',
		'menu_title' 	=> 'Temos nustatymai',
		'redirect' 		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Sekcijų pavadinimai',
		'menu_title' 	=> 'Sekcijų pavadinimai',
		'parent_slug' 	=> $parent['menu_slug'],
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Skaitikliai',
		'menu_title' 	=> 'Skaitikliai',
		'parent_slug' 	=> $parent['menu_slug'],
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Karuselės nustatymai',
		'menu_title' 	=> 'Karuselės nustatymai',
		'parent_slug' 	=> $parent['menu_slug'],
	));


}
