<?php 
    $sliderArgs = array( 'post_type' => 'tandem_slides', 'posts_per_page' => -1 );
    $sliderQuery =  new WP_Query( $sliderArgs );
    if($sliderQuery->have_posts()):
?>
    
<section class="slider">
    <button type="button" class="slider__button slider__button--left">
    <span class="slider__button__text"><?php _e('Rodyti ankstenę skaidrę.', 'tandem'); ?></span>     
    </button>
    <!-- https://github.com/akiran/react-slick -->
    <ul class="slider__wrapper">

    <?php
    while ( $sliderQuery->have_posts() ) : $sliderQuery->the_post();
    ?>
    <li class="slider__slide">
        <img class="slider__slide__image" src="<?php the_field('slide_photo'); ?>" alt="<?php The_title(); ?>">
        <div class="slider__slide__content">
            <h3 class="slider__slide__title"><?php The_title(); ?></h3>
            <div class="slider__slide__description"><?php the_field('slide_subtext'); ?></div>
        </div>
    </li>
    <?php
    endwhile;
    ?>
                    
    </ul>
    <button type="button" class="slider__button slider__button--right">
    <span class="slider__button__text"><?php _e('Rodyti sekančią skaidrę.', 'tandem'); ?></span>   
    </button>
</section>

<?php
    endif;
?>
