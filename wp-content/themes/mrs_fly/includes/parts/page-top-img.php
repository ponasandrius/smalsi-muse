<section class="header-image" id="js-top_page_img" data-main-img="<?php echo (get_field('head_img')?get_field('head_img'):get_field('head_img','option')); ?>" data-mob-img="<?php echo (get_field('head_img_mob')?get_field('head_img_mob'):get_field('head_img_mob','option')); ?>">
    <div class="header-image__content-holder">       
        <?php
        
        if(get_field('show_content_in_header')){
            ?>
            <div class="header-image__content">
            <?php
                if ( have_posts() ) { $count = 0;
                    while ( have_posts() ) { the_post(); $count++;
                        ?>
                            <?php the_content(); ?>
                    <?php
                    } // End WHILE Loop
                }
            ?>
            </div>
            <?php
        }
        ?>
    </div>
</section><!-- end of #top_page_img -->
