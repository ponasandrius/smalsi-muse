<div class="cont-map" id="map"></div>
            <script>
                  function initMap() {//,,17z
                      var myLatLng = {lat: <?php the_field('adresas_latitude','options'); ?>, lng: <?php the_field('adresas_longitude','options'); ?>};
                    var mapDiv = document.getElementById('map');
                    var map = new google.maps.Map(mapDiv, {
                        center: {lat: <?php the_field('adresas_latitude','options'); ?>, lng: <?php the_field('adresas_longitude','options'); ?>},
                        zoom: 16,
                        scrollwheel: false,
                        navigationControl: false,
                        mapTypeControl: false,
                        scaleControl: false,
                        draggable: false
                    });

                    var marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      icon: "<?php echo ELN.'img/pin.png'; ?>",
                      title: 'DS group'
                    });
                  }
                </script>
                <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIfQpTzmrDpZUyatp05JiFwYEs6l6ms8A&callback=initMap">
                </script>