<?php
$tips_args = array('post_type' => 'tandem_tips', 'posts_per_page' => get_field('tips_section_count', 'option'));

// The Query
$tips_query = new WP_Query( $tips_args );
?>
<section class="trip-tips">
        <h2 class="heading-2"><?php the_field('tips_section_title', 'option'); ?></h2>
        <h3 class="heading-3"><?php the_field('tips_section_subtitle', 'option'); ?></h3>

        <?php
        if ( $tips_query->have_posts() ) : 
        ?>
        <div class="cards-slider">
            <div class="cards-slider__wrapper">
                <button type="button" class="cards-slider__navigation cards-slider__navigation--left"></button>

                <ul class="cards-slider__cards-wrapper" id="js-cards-slider__cards-wrapper--main-page">
                <?php
                while ( $tips_query->have_posts() ) :
                $tips_query->the_post();
                ?>
                    <li class="cards-slider__card">
                        <div class="cards-slider__card-wrapper">
                            <div class="cards-slider__card__image">
                                <img src="<?php echo get_the_post_thumbnail_url($tips_query->post->ID, 'medium-img');?>" alt="<?php the_title(); ?>">
                            </div>
                            <div class="cards-slider__card__content">
                                <h3><?php the_title(); ?></h3>
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                    </li>

                    <?php endwhile; ?>
                </ul>

                <button type="button" class="cards-slider__navigation cards-slider__navigation--right"></button>
            </div> 
            
            <div class="buttons-wrapper buttons-wrapper--right">
                <a href="<?php echo get_permalink(get_field('tips_section_archive', 'option')); ?>" class="btn"><?php the_field('tips_section_button_text', 'option'); ?></a>
            </div>        
        </div>
        <?php  wp_reset_postdata(); endif; ?>   
    </section>