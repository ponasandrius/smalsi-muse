<?php
$tops_args = array('post_type' => 'tandem_tops', 'posts_per_page' => get_field('tops_section_count', 'option'));

// The Query
$tops_query = new WP_Query( $tops_args );
?>
<section class="trip-top">
    <h2 class="heading-2"><?php the_field('tops_section_title', 'option'); ?></h2>
    <h3 class="heading-3"><?php the_field('tops_section_subtitle', 'option'); ?></h3>
    <?php
    if ( $tops_query->have_posts() ) : 
    ?>
    <ul class="horizontal-layout">
    <?php
    while ( $tops_query->have_posts() ) :
    $tops_query->the_post();
    ?>
        <li class="horizontal-layout__item">
            <div class="horizontal-layout__image">
                <img src="<?php echo get_the_post_thumbnail_url($tops_query->post->ID);?>" alt="<?php the_title(); ?>">
            </div>
            <div class="horizontal-layout__content">
                <h3 class="horizontal-layout__heading"><?php the_title(); ?></h3>
                <?php the_excerpt(); ?>
            </div>
        </li>
    <?php endwhile; ?>
    </ul>
    <div class="buttons-wrapper buttons-wrapper--right">
        <a href="<?php echo get_permalink(get_field('tops_section_archive', 'option')); ?>" class="btn"><?php the_field('tops_section_button_text', 'option'); ?></a>
    </div>
    <?php  wp_reset_postdata(); endif; ?>
</section>