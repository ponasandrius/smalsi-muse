<main class="trips-list">
    <h2 class="heading-2"><?php the_field('trips_section_title', 'option'); ?></h2>
    <h3 class="heading-3"><?php the_field('trips_section_subtitle', 'option'); ?></h3>
    
    <?php
    function getTripClass($is_first, $is_third) {
        if($is_first == 1) {
            return 'trips-list__item trips-list__item--horizontal';
        }

        if($is_third == 3) {
            return 'trips-list__item trips-list__item--vertical';
        }

        return 'trips-list__item';
    }

    $trips_args = array('post_type' => 'tandem_trips', 'posts_per_page' => 6);

    // The Query
    $trips_query = new WP_Query( $trips_args );

    // The Loop
    if ( $trips_query->have_posts() ) : 
    $is_first = 0;
    $is_third = 0;
    ?>
    <ul class="trips-list__wrapper">
    <?php
        while ( $trips_query->have_posts() ) :
            $trips_query->the_post();
            $is_first++;
            $is_third++;
            if($is_first == 6) {
                $is_first = 0;
            }

            if($is_third == 6) {
                $is_third = 0;
            }

            $tripCategories = get_the_terms($trips_query->post->ID, 'trip_cat');
            if(isset($tripCategories[0])):
                $tripCategory = $tripCategories[0];
                $tripCategoryIconUrl = get_field('category_icon', $tripCategory);
                $tripCategoryClass = get_field('category_class', $tripCategory);
                $tripCategoryTitle = $tripCategory->name;
                $tripClass = getTripClass($is_first, $is_third);

                // Get length of the trip
                 $tripLengths = get_the_terms($trips_query->post->ID, 'trip_length');
                 $tripLength = isset($tripLengths[0]) ? $tripLengths[0]->name : '';
                 ?>
                <li class="<?php echo $tripClass . ' ' . 'trips-list__item--' . $tripCategoryClass; ?>" >
                    <div class="trips-list__item__content" style="background: url(<?php echo get_template_directory_uri();?>/public/images/a1.jpg) center center no-repeat; background-size: cover;">
                        <div class="trips-list__item__title__wrapper">          
                            <h3 class="trips-list__item__title"><?php the_title(); ?></h3>
                            <span class="trips-list__item__length"><?php echo $tripLength; ?></span>
                        </div>
                        <div class="trips-list__item__description">
                            <div class="trips-list__item__description__content">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                        <a href="" class="trips-list__item__type-icon">
                            <img src="<?php echo $tripCategoryIconUrl; ?>" alt="Kategorija - <?php echo $tripCategoryTitle; ?>">
                            <span class="trips-list__item__type-icon__title"><?php echo $tripCategoryTitle; ?></span>
                        </a>
                    </div>
                </li>
            <?php
            endif;
        endwhile;
        ?>
        </ul>
        <?php
        /* Restore original Post Data */
        wp_reset_postdata();
    else:
        
    endif;
    ?>

    <div class="buttons-wrapper buttons-wrapper--right">
        <a href="<?php echo get_permalink(get_field('trips_archive', 'option')); ?>" class="btn"><?php the_field('trips_button_text', 'option'); ?></a>
    </div>
</main>