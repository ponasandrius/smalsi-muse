    
    <footer class="footer">
        <div class="footer__wrapper">
          <div class="footer__contacts">
            <div class="footer__contacts__icon">
              <i class="fa fa-comments-o" aria-hidden="true"></i>
            </div>
            <div class="footer__contacts__content">
              <h6>Kontaktai</h6>
              <div>info@smalsimuse.lt</div>
              <div>+370686868686</div>
            </div>
          </div>
          <div class="footer__social">
            <a href="" class="footer__social__icon footer__social__icon--facebook">
              <i class="fa fa-facebook" aria-hidden="true"></i>
            </a>
            <a href="" class="footer__social__icon footer__social__icon--instagram">
              <i class="fa fa-instagram" aria-hidden="true"></i>
            </a>
          </div>
          <div class="footer__newsletter">
            <div class="newsletter-form">
              <form action="">
                <label>Gauk pirmasis naują kelionės maršrutą!</label>
                <div class="newsletter-form__input-wrapper">
                  <input type="text" class="newsletter-form__input">
                  <submit class="btn btn--submit"><i class="fa fa-envelope-o" aria-hidden="true"></i></submit>
                </div>
              </form>
            </div>
          </div>
        </div>
    </footer>
    
    <?php wp_footer(); ?>
  </body>
</html>
