module.exports = function () {
	
	var config = {
			path: {
				HTML: '/index.html',
				ALL: ['assets/js/**/*.jsx', 'assets/js/**/*.js'],
				MINIFIED_OUT: 'bundle.min.js',
				DEST_SRC: 'public',
				DEST_BUILD: 'public',
				DEST: 'dist',
			},
			
			scss: {
				src: [
					'./assets/scss/**/*.scss',
					'!assets/scss/**/*_scsslint_tmp*.scss', //ignores temporary scss-lint files
				],
				cssFolder: 'public/styles/',
			}
			,
			optimize: {
				css: {}
				,
				js: {}
				,
			},
		};

	return config;
};



