<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php get_header(); ?>
<?php get_template_part('includes/parts/page-top-img'); ?>
<?php get_template_part('includes/parts/page-breadcrumb'); ?>
<main id="page-content" class="row">

    <?php
        if ( have_posts() ) { $count = 0;
            while ( have_posts() ) { the_post(); $count++;
                ?>
                 <?php get_template_part('content'); ?>
            <?php
            } // End WHILE Loop
        } else {
    ?>
        <article <?php post_class(); ?>>
            <p><?php _e('Turinys nerastas','tandem'); ?></p>
        </article><!-- /.post -->
    <?php } // End IF Statement ?>
</main><!-- end of #page-content -->
<?php get_footer(); ?>
