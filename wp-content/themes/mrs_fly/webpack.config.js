let path = require('path');

module.exports = {
	cache: true,
	debug: true,
	devServer: {
		contentBase: './static',
		hot: true,
		stats: {
			colors: true,
		},
	},
	devtool: 'eval',
	entry: {
        bundle: ['./assets/js/main.js'],
    },
	output: {
        filename: '[name].js',
        path: path.resolve(__dirname, '../public'),
	},
	resolve: {
		extensions: ['', '.js', '.json', '.coffee'],
	},
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['react-hot-loader', 'babel-loader'],
                exclude: /node_modules/,
            },
        ],
    },
};
