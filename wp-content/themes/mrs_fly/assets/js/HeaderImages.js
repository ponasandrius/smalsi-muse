import debounce from './debounce';

export default class HeaderImages {
    constructor($) {
        this.$ = $;
        this.$headerImageContainer = this.$('#js-top_page_img');
        this.breakPoint = 750;
        this.displaySmall = 0;

        this.initialise = this.initialise.bind(this);
        this.loadImage = this.loadImage.bind(this);
        this.handleLoad = this.handleLoad.bind(this);
        this.handleScreenSizeChange = this.handleScreenSizeChange.bind(this);
        this.debouncedHandleScreenSizeChangel = debounce(this.handleScreenSizeChange, 100);

        if(this.$headerImageContainer) {
            this.initialise();
        }
    }

    initialise() {
        this.bigImg = this.$headerImageContainer.data('main-img');
        this.smallImg = this.$headerImageContainer.data('mob-img');
        window.addEventListener('resize', this.debouncedHandleScreenSizeChangel);
        this.handleScreenSizeChange();
    }

    loadImage(image) {
        const that = this;
        const tempImg = this.$('<img src="' + image +'" />');
        tempImg.load(this.handleLoad);
    }

    handleLoad(image) {
        this.$headerImageContainer.css('background-image', 'url(' + image.target.currentSrc + ')');
    }

    handleScreenSizeChange() {
         console.log(this.displaySmall, this.bigImg);
        if(this.$(window).width() > this.breakPoint ) {
            if(this.displaySmall !== 1) {
                this.displaySmall = 1;
                this.loadImage(this.bigImg);
            }
        }else if(this.displaySmall !== 2){
            this.displaySmall = 2;
            this.loadImage(this.smallImg);
        }
    }
}