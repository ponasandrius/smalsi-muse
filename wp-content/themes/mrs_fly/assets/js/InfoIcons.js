import debounce from './debounce';

export default class InfoIcons {
    constructor($) {
        this.$ = $;
        this.$countersWrapper = $('.info-icons__wrapper');
        this.$counters = $('.info-icon');
        this.animated = false;

        this.initialise = this.initialise.bind(this);
        this.onScroll = this.onScroll.bind(this);
        this.animate = this.animate.bind(this);
        this.increCounter = this.increCounter.bind(this);
        this.initCounter = this.initCounter.bind(this);
        this.handleScreenSizeChange = this.handleScreenSizeChange.bind(this);

        this.debouncedOnScroll = debounce(this.onScroll, 100);
        this.debouncedHandleScreenSizeChangel = debounce(this.handleScreenSizeChange, 100);

        if(this.$counters.length) {
            this.initialise();
        }

        
    }

    initialise() {
        this.topBoundary = this.$(this.$countersWrapper).offset().top;
        this.boxHeight = this.$(this.$countersWrapper).height();
        this.wH = this.$(window).height();
        this.windowScroll = this.$('body').scrollTop();

        document.addEventListener('scroll', this.debouncedOnScroll);
        window.addEventListener('resize', this.debouncedHandleScreenSizeChangel);
    }

    onScroll() {
        this.windowScroll = this.$('body').scrollTop();
        if(this.windowScroll > (this.topBoundary - this.wH + (this.boxHeight / 2)) && 
        this.windowScroll < (this.topBoundary + (this.boxHeight / 2))) {
            this.animate();
        }
    }

    handleScreenSizeChange() {
        this.topBoundary = this.$(this.$countersWrapper).offset().top;
        this.boxHeight = this.$(this.$countersWrapper).height();
        this.wH = this.$(window).height();
        this.windowScroll = this.$('body').scrollTop();

        console.log('resized');
    }

    animate() {
        if(this.animated) {
            return;
        }

        this.animated = true;
        document.removeEventListener('scroll', this.debouncedOnScroll);
        window.removeEventListener('resize', this.debouncedHandleScreenSizeChangel);

        this.$counters.each(this.initCounter);
    }

    initCounter(i, counter) {
        this.increCounter(this.$(counter), 0, this.$(counter).data('count'), parseInt(this.$(counter).data('count')) / 500);
    }

    increCounter($counter, count, max, incre) {
        const that = this;
        $counter.find('.info-icon__count').html(count);
        if(count <= max) {
            setTimeout(function() {
                that.increCounter($counter, Math.ceil(count + incre), max, incre);
            }, 1);
        }
    }

}

