import Filters from './Filters';
import Slider from './Slider';
import CardsSlider from './CardsSlider';
import InfoIcons from './InfoIcons';
import MainMenu from './MainMenu';
import HeaderImages from './HeaderImages';

jQuery(document).ready(function($) {
    const filterMenu = new Filters($);
    const slider = new Slider($);
    const cardsSlider = new CardsSlider($);
    const infoIcons = new InfoIcons($);
    const mainMenu = new MainMenu($);
    const headerImage = new HeaderImages($);
});