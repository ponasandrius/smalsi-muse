class Filters {
    constructor($) {
        this.$ = $;

        this.$filtersToggle = this.$('#js-filters-toggle');
        this.$filtersMenu = this.$('#js-filters-menu');
        this.$filtersList = this.$('#js-filters-menu__filters-form');
        this.$countriesList = this.$('#js-filters-menu__countries-form');

        this.$filtersListWrapper = this.$('#js-filters-menu__filters');
        this.$countriesListWrapper = this.$('#js-filters-menu__countries');

        this.$filtersButton = $('#js-display-filtered-trips');

        this.menuIsOpen = false;

        this.handleMenuToggle = this.handleMenuToggle.bind(this);
        this.handleGetFiltersResponseSuccess = this.handleGetFiltersResponseSuccess.bind(this);
        this.handleFiltersChange = this.handleFiltersChange.bind(this);
        this.handleCountriesLoaded = this.handleCountriesLoaded.bind(this);
        this.addCountry = this.addCountry.bind(this);
        this.handleDisplayTrips = this.handleDisplayTrips.bind(this);        

        if(this.$filtersToggle) {
            this.$filtersToggle.on('click', this.handleMenuToggle);
            this.$filtersButton.on('click', this.handleDisplayTrips).attr('disabled', 'disabled');
        }
    }

    handleMenuToggle(e) {
        e.preventDefault();
        this.menuIsOpen = !this.menuIsOpen;

        if(this.menuIsOpen) {
            this.$filtersMenu.fadeIn(300);
            this.getFilters();
            this.$filtersToggle.addClass('active');
        }else{
            this.$filtersMenu.fadeOut(300);
            this.$filtersToggle.removeClass('active');
        }
    }

    getFilters() {
        const that = this;
        const data = {
			'action': 'load_trips_filters'
		};

        this.$filtersListWrapper.addClass('loading');
        this.$filtersList.html('');
        this.$.post(MyAjax.ajaxurl, data, this.handleGetFiltersResponseSuccess).fail(function(error) {
                console.error(error.responseText);
                that.$filtersListWrapper.removeClass('loading');
            });
    }

    handleGetFiltersResponseSuccess(response) {
        this.$filtersListWrapper.removeClass('loading');

        for(var i = 0; i < response.length; i++) {

            if(response[i].terms.length === 0) {
                return;
            }
            var newElement = this.$('<section class="filters-menu__section"></section>');
            newElement.append('<h3>'+ response[i].label +':</h3>');

            for(var j = 0; j < response[i].terms.length; j++) {
                
                var filter = this.$('<div class="filters-menu__filter">' + 
                                    '<input type="checkbox" value="'+ response[i].terms[j].term_id +'" id="filter-'+ response[i].name +'-'+ response[i].terms[j].slug+'" name="filter-'+ response[i].name +'"/>' +
                                        '<label for="filter-'+ response[i].name +'-'+ response[i].terms[j].slug +'">' +
                                            response[i].terms[j].name +
                                        '</label>'+
                                    '</div>');

                filter.on('change', this.handleFiltersChange);
                newElement.append(filter);
            }

            this.$filtersList.append(newElement);
        }
    }

    handleFiltersChange() {
        const that = this;
        this.$countriesListWrapper.addClass('loading');
        let data = {'action': 'load_countries', 'filters': {}};

        this.$filtersList.find(':checked').each(function() {
            if(data.filters[that.$(this).attr('name')] === undefined) {
                data.filters[that.$(this).attr('name')] = [];
            }
            data.filters[that.$(this).attr('name')].push(that.$(this).val());
        });

        this.$.post(MyAjax.ajaxurl, data, this.handleCountriesLoaded).fail(function(error) {
            console.error(error.responseText);
            that.$countriesListWrapper.removeClass('loading');
        });
    }

    handleCountriesLoaded(response) {
        this.$countriesListWrapper.removeClass('loading');        

        if(response.countries) {                    
            this.$('.filters-menu__countries-form').html('');
            this.$.each(response.countries, this.addCountry);
            if(response.countries) {
                this.$filtersButton.removeAttr('disabled');
            }else{
                this.$filtersButton.attr('disabled', 'disabled');
            }
        }
    }

    addCountry(i, country) {
        let countryElement = this.$('<div style="display:none;" class="filters-menu__filter  filters-menu__filter--country">' + 
                                '<input type="checkbox" checked value="'+ country.id +'" id="country-'+ country.slug +'" name="coutry-'+ country.slug +'"/>' +
                                    '<label for="country-'+ country.slug +'">' +
                                        country.name +
                                    '</label>'+
                                '</div>');
        this.$countriesList.append(countryElement);
        countryElement.fadeIn();
    }

    handleDisplayTrips(e) {
        e.preventDefault();
        const that = this;
        let data = {'countries': [], 'filters': {}};

        this.$filtersList.find(':checked').each(function() {
            if(data.filters[that.$(this).attr('name')] === undefined) {
                data.filters[that.$(this).attr('name')] = [];
            }
            data.filters[that.$(this).attr('name')].push(that.$(this).val());
        });

        this.$countriesList.find(':checked').each(function() {
            data.countries.push(that.$(this).val());
        });

        if(data.countries.length > 0) {
            window.location.href = MyAjax.tripsArchiveURL + '?' + serialize(data);
        }
    }
};

function serialize(obj, prefix) {
  var str = [], p;
  for(p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
      str.push((v !== null && typeof v === "object") ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}

export default Filters;