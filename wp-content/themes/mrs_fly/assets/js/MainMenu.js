import debounce from './debounce';

export default class MainMenu {
    constructor($) {
        this.$ = $;

        this.menuToggle = $('.navigation-main__toggle');
        this.header = $('.main-header');

        this.menuIsOpen = false;
        this.headerIsShrinked = false;

        this.windowScroll = this.$('body').scrollTop();

        this.handleScroll = this.handleScroll.bind(this);
        this.debouncedHandleScroll = debounce(this.handleScroll, 100);
        this.handleToggleClick = this.handleToggleClick.bind(this);
        this.handleScreenSizeChange = this.handleScreenSizeChange.bind(this);
        this.debouncedhandleScreenSizeChange = debounce(this.handleScreenSizeChange, 100);

        document.addEventListener('scroll', this.debouncedHandleScroll);
        window.addEventListener('resize', this.debouncedhandleScreenSizeChange);

        this.menuToggle.on('click', this.handleToggleClick);
    }

    handleScroll() {
        this.windowScroll = this.$('body').scrollTop();
        
        if(this.windowScroll > 0) {
            if(!this.headerIsShrinked) {
                this.header.addClass('main-header--sticky');
                this.headerIsShrinked = true;
            }
        }else if(this.headerIsShrinked) {
            this.header.removeClass('main-header--sticky');
            this.headerIsShrinked = false;
        }
    }

    handleToggleClick(e) {
        e.preventDefault();
        this.menuToggle.toggleClass('navigation-main__toggle--open');
        this.menuIsOpen = !this.menuIsOpen;
        if(this.menuIsOpen) {
            this.$('.navigation-main__wrapper').slideDown();
        }else{
            this.$('.navigation-main__wrapper').slideUp();
        }
    }

    handleScreenSizeChange(e) {
        if(this.$(window).width() > 1210 && !this.menuIsOpen) {
            this.handleToggleClick(e);
        }
    }
}