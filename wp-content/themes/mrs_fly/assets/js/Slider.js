class Slider {
    constructor($) {

        const sliderElement = $('.slider__wrapper');

        if(sliderElement.length === 0) {
            return;
        }

        sliderElement.slick({
            arrows: true,
            prevArrow: $('.slider__button--left'),
            nextArrow: $('.slider__button--right'),
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });
    }
}

export default Slider;