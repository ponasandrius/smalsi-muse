class CardsSlider {
    constructor($) {

        const cardsSlider = $('#js-cards-slider__cards-wrapper--main-page');

        if(cardsSlider.length == 0) {
            return;
        }

        $('.cards-slider__cards-wrapper').slick({
            slidesToShow: 3,
            arrows: true,
            prevArrow: $('.cards-slider__navigation--left'),
            nextArrow: $('.cards-slider__navigation--right'),
            responsive: [
                {
                    breakpoint: 1224,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 700,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
}

export default CardsSlider;