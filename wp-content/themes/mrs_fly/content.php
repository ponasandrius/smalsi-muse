<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>

<article <?php post_class('main-container__content-wrapper'); ?>>
        <div class="main-container__content">
            <?php            
                the_content();
            ?>
        
            <?php edit_post_link( __( '{ Redaguoti }', 'tandem' ), '<span class="small">', '</span>' ); ?>
        </div>
</article>
