<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<h1 class="page-title"><?php the_title(); ?></h1>
<article class="row">
    <div class="column medium-offset-3 medium-6" id="inner_content">
        <?php the_content(); ?>
        <?php edit_post_link( __( '{ Redaguoti }', 'tandem' ), '<span class="small">', '</span>' ); ?>
    </div>
</article>
<div class="row">
    <div class="column medium-offset-3 medium-6" id="service-bottom-nav">
        <a href="" class="prev_page prev"><span class="arr_icon"><span class="icon-wrap"></span></span>visos paslaugos</a><a href="" class="next_page next">Sekanti paslauga<span class="arr_icon"><span class="icon-wrap"></span></span></a>
    </div>
</div>
