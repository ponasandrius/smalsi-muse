<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<h1 class="page-title column"><?php the_title(); ?></h1>
<article class="<?php post_class('row'); ?>">
    <div class="column large-5" id="inner_content">
        <?php 
        
            if(get_field('ijungti_paslaugu_punktus')){
                        if( have_rows('paslaugu_punktai') ):
                            echo '<ul class="pasl-pu">';
                            // loop through the rows of data
                            while ( have_rows('paslaugu_punktai') ) : the_row();
                                
                                echo '<li>'.get_sub_field('punkto_pavadinimas').'<div class="pasl-pu--cont">'.get_sub_field('punkto_aprasymas').'</div></li>';  
                            endwhile;
                            echo '</ul>';
                        endif;
                    }else{
                        the_content();
                    }
        
        
        ?>
        <?php edit_post_link( __( '{ Redaguoti }', 'tandem' ), '<span class="small">', '</span>' ); ?>
    </div>
    <div class="column large-5 large-offset-2" id="main_cont_form">
                           
                            <?php the_field('conts_content'); ?>
                        </div>
</article>
