<?php
/**
 * Template Name: Produktai
 *
 *
 *
 */
?>
<?php get_header(); ?>
<?php get_template_part('includes/parts/page-top-img'); ?>
<?php get_template_part('includes/parts/page-breadcrumb'); ?>
<?php $categories = get_terms( 'prod_cat', 'hide_empty=0' ); ?>
<section class="row">
    <?php get_template_part('includes/parts/product-sidebar'); ?>
    <div class="column large-9">
       <div class="row">
       <?php
            foreach($categories as $cat){
                $cat_link = get_term_link( $cat );
                if(!$cat->parent && $cat->parent < 1){
                ?>
                  <div class="column medium-4 large-4 main_prod">
                        <a href="<?php echo esc_url( $cat_link ); ?>" title="<?php echo $cat->name; ?>" style="background: url(<?php if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($cat->term_id); ?>) center center no-repeat; background-size:cover;">
                            <h3><?php echo $cat->name; ?></h3>
                        </a>
                    </div>
                <?php
                }
            }// end foreach
        ?>


        </div>
    </div>
</section><!-- end of #page-content -->

<?php get_footer(); ?>
