<?php
/**
 * Template Name: Paslaugos
 *
 *
 *
 */
?>
<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php get_header(); ?>
<?php get_template_part('includes/parts/page-top-img'); ?>
<?php get_template_part('includes/parts/page-breadcrumb'); ?>
<main id="page-content" class="row">
    <div class="column">
        <?php

        // check if the repeater field has rows of data
        if( have_rows('paslaugos','option') ):
            
            // loop through the rows of data
            while ( have_rows('paslaugos','option') ) : the_row();
                $page = get_sub_field('paslaugos_puslapis');
                //print_r($page);
                ?>
                <div class="row single-service-link">
                   
                    <div class="columns small-4 medium-3">
                        <a href="<?php the_permalink($page->ID); ?>" class="service-icon"><img src="<?php the_sub_field('ikona'); ?>" alt="<?php echo $page->post_title; ?>"></a>
                    </div>
                    
                    <div class="columns small-8 medium-9">
                        <a href="<?php the_permalink($page->ID); ?>" class="">
                            <h3><?php echo __($page->post_title); ?></h3>
                        </a>
                        <div class="single-service__content">
                            <?php _e($page->post_excerpt_ml); ?>
                        </div>
                        <div class="text-right">
                            <a href="<?php the_permalink($page->ID); ?>" class="btn"><?php _e('Plačiau', 'tandem'); ?></a>
                        </div>
                    </div>
                    
                </div>
                <?php
                // display a sub field value
                

            endwhile;

        else :

            // no rows found

        endif;

        ?>
    </div>
     
</main><!-- end of #page-content -->

<?php get_footer(); ?>
