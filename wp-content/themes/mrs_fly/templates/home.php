<?php
/**
 * Template Name: Pradinis
 *
 *
 *
 */
?>
<?php get_header(); ?>

<?php // load slider section ?>
<?php get_template_part('includes/parts/main-page-slider'); ?>
<?php get_template_part('includes/parts/main-page-cats'); ?>

<section class="about-us-section">
  <div class="row">
      <div class="column">
          <h2 class="section-title">Apie mus</h2>
      </div>
  </div>   
    <div class="row about-content">
        <div class="column medium-5"><div class="about-image"></div></div>
        <div class="column medium-7 about-cont-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, laborum maiores eum eius perspiciatis inventore consequuntur corporis, harum, enim unde tempore cupiditate cum omnis quae, earum obcaecati? Doloribus, labore sequi?</div>
    </div>
</section>

<?php // load production section ?>
<?php get_template_part('includes/parts/main-page-products'); ?>

<section class="instagram-section">
    <div class="row">
        <div class="column">
            <h2 class="section-title"><?php _e('Instagram','tandem'); ?></h2>
        </div>
    </div>
    <div class="row column-holder">
        <div class="column">
            <?php echo do_shortcode('[instagram-feed]'); ?>
            <div class="insta-more text-right">
                <a href="<?php the_field('instagram_nuoroda','options'); ?>" target="_blank" class="btn"><?php _e('Žiūrėti visas nuotraukas','tandem'); ?></a>
            </div>
        </div>
    </div>
</section>

<section class="contacts-section">
    <div class="row contacts-row">
        <div class="column medium-5 contacts-content">
           <div class="contact-content--wrapper">
               <div><?php the_field('darbo_laikas','options'); ?></div>
                <div><?php the_field('adresas','options'); ?></div>
                <div><?php the_field('tel','options'); ?></div>
           </div>
            
        </div>
        <div class="column medium-7">
            <div class="cont-map" id="map"></div>
            <script>
                  function initMap() {//,,17z
                      var myLatLng = {lat: <?php the_field('adresas_latitude','options'); ?>, lng: <?php the_field('adresas_longitude','options'); ?>};
                    var mapDiv = document.getElementById('map');
                    var map = new google.maps.Map(mapDiv, {
                        center: {lat: <?php the_field('adresas_latitude','options'); ?>, lng: <?php the_field('adresas_longitude','options'); ?>},
                        zoom: 16,
                        scrollwheel: false,
                        navigationControl: false,
                        mapTypeControl: false,
                        scaleControl: false,
                        draggable: false
                    });

                    var marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      icon: "<?php echo ELN.'img/pin.png'; ?>",
                      title: 'DS group'
                    });
                  }
                </script>
                <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIfQpTzmrDpZUyatp05JiFwYEs6l6ms8A&callback=initMap">
                </script>
        </div>
    </div>
</section>

<?php get_template_part('includes/parts/main-page-map'); ?>

<?php get_footer(); ?>
