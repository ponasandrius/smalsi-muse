<?php
/**
 * Template Name: Patarimai
 *
 *
 *
 */
?>
<?php get_header(); ?>
<?php get_template_part('includes/parts/page-top-img'); ?>
<?php
$tips_args = array('post_type' => 'tandem_tips', 'posts_per_page' => -1);

// The Query
$tips_query = new WP_Query( $tips_args );
?>
<main class="main-container">
    <h1 class="heading-2"><?php the_title(); ?></h1>
    <h3 class="heading-3"><?php the_field('subtitle'); ?></h3>

    <?php
        if ( $tips_query->have_posts() ) : 
        ?>
        <div class="cards-slider">
            <div class="cards-slider__wrapper">

                <ul class="cards-slider__cards-wrapper cards-slider__cards-wrapper--page">
                <?php
                while ( $tips_query->have_posts() ) :
                $tips_query->the_post();
                ?>
                    <li class="cards-slider__card">
                        <div class="cards-slider__card-wrapper">
                            <div class="cards-slider__card__image">
                                <img src="<?php echo get_the_post_thumbnail_url($tips_query->post->ID, 'medium-img');?>" alt="<?php the_title(); ?>">
                            </div>
                            <div class="cards-slider__card__content">
                                <h3><?php the_title(); ?></h3>
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                    </li>

                    <?php endwhile; ?>
                </ul>
            </div>      
        </div>
        <?php  wp_reset_postdata(); endif; ?>   
</main><!-- end of #page-content -->

<?php get_footer(); ?>
