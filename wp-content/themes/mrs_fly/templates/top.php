<?php
/**
 * Template Name: TOP
 *
 *
 *
 */
?>
<?php get_header(); ?>
<?php get_template_part('includes/parts/page-top-img'); ?>
<?php
$tops_args = array('post_type' => 'tandem_tops', 'posts_per_page' => -1);

// The Query
$tops_query = new WP_Query( $tops_args );
?>
<main class="main-container">
    <h1 class="heading-2"><?php the_title(); ?></h1>
    <h3 class="heading-3"><?php the_field('subtitle'); ?></h3>

    <?php
    if ( $tops_query->have_posts() ) : 
    ?>
    <ul class="horizontal-layout">
    <?php
    while ( $tops_query->have_posts() ) :
    $tops_query->the_post();
    ?>
        <li class="horizontal-layout__item">
            <div class="horizontal-layout__image">
                <img src="<?php echo get_the_post_thumbnail_url($tops_query->post->ID);?>" alt="<?php the_title(); ?>">
            </div>
            <div class="horizontal-layout__content">
                <h3 class="horizontal-layout__heading"><?php the_title(); ?></h3>
                <?php the_excerpt(); ?>
            </div>
        </li>
    <?php endwhile; ?>
    </ul>
    <?php  wp_reset_postdata(); endif; ?>
</main><!-- end of #page-content -->

<?php get_footer(); ?>
