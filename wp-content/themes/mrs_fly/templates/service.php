<?php
/**
 * Template Name: Paslauga
 *
 *
 *
 */
?>
<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php get_header(); ?>
<?php get_template_part('includes/parts/page-top-img'); ?>
<?php get_template_part('includes/parts/page-breadcrumb'); ?>
<main id="page-content" class="row">

    <?php
        if ( have_posts() ) { $count = 0;
            while ( have_posts() ) { the_post(); $count++;
                ?>
                <div class="column medium-5" id="inner_content">
                    <?php 
                        $pascontents = '';
                        $pascounter = 1;
                        if(get_field('ijungti_paslaugu_punktus')){
                                if( have_rows('paslaugu_punktai') ):
                                    echo '<ul class="pasl-pu">';
                                    // loop through the rows of data
                                    while ( have_rows('paslaugu_punktai') ) : the_row();

                                        echo '<li id="pasl-'.$pascounter.'" class="'.($pascounter == 1?'open':'').'">'.get_sub_field('punkto_pavadinimas').'</li>';

                                        $pascontents .= '<div class="pasl-pu--cont" id="pasl-pu-'.$pascounter++.'">'.get_sub_field('punkto_aprasymas').'</div>';
                                    endwhile;
                                    echo '</ul>';
                                endif;
                            }
                    ?>
                    <?php edit_post_link( __( '{ Redaguoti }', 'tandem' ), '<span class="small">', '</span>' ); ?>
                </div>
                <div class="column medium-6 medium-offset-1" id="main_cont_form">         
                    <?php echo $pascontents; ?>
                </div>
            <?php
            } // End WHILE Loop
        } else {
    ?>
        <article <?php post_class(); ?>>
            <p><?php _e('Turinys nerastas','tandem'); ?></p>
        </article><!-- /.post -->
    <?php } // End IF Statement ?>
</main><!-- end of #page-content -->
<?php get_footer(); ?>