<?php
/**
 * Template Name: Straipsniai
 *
 *
 *
 */
?>
<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php get_header(); ?>
<?php get_template_part('includes/parts/page-top-img'); ?>
<?php get_template_part('includes/parts/page-breadcrumb'); ?>
<main id="page-content" class="row">

    <?php
        if ( have_posts() ) { $count = 0;
            while ( have_posts() ) { the_post(); $count++;
                ?>
                 <?php get_template_part('content'); ?>
            <?php
            } // End WHILE Loop
        } else {
    ?>
        <article <?php post_class(); ?>>
            <p><?php _e('Turinys nerastas','tandem'); ?></p>
        </article><!-- /.post -->
    <?php } // End IF Statement ?>
</main><!-- end of #page-content -->
<div class="row" data-equalizer data-equalize-on="medium">
         <div class="column">
          <?php
                   $args3 = array(
                    'posts_per_page' => '-1',
                    'post_type'   => 'post'
                  );

                  $qw = new WP_Query($args3);
                   if ($qw->have_posts()) {
                        while ($qw->have_posts()) {
                            $qw->the_post();

                            ?>
                            <article class="row mp-news dark">
                             <?php $thumbnail = get_the_post_thumbnail_url(); ?>
                             <?php if($thumbnail): ?>
                              <div class="column medium-3">
                                 <a href="<?php the_permalink(); ?>">
                                      <img src="<?php echo $thumbnail; ?>" alt="<?php the_title(); ?>">
                                  </a>
                              </div>
                              <?php endif; ?>
                               <div class="news-cont column medium-9" data-equalizer-watch>
                                   <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                   <?php the_excerpt(); ?>
                                   <a class="btn" href="<?php the_permalink(); ?>"><?php _e('Plačiau', 'tandem'); ?></a>
                               </div>                               
                           </article>
                            <?php
                        }
                   }
                    wp_reset_query();
                ?>
        </div>
</div>
<?php get_footer(); ?>
