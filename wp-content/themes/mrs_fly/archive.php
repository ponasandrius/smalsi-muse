<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php get_header(); ?>
<?php get_part('page-top-img'); ?>
<section id="page">
   <article id="page-content" class="row">
  <div class="column small-12">
       <div class="h1-wrapper text-center">
           <h1><?php _e('Puslapis, kurio ieškote nerastas','tandem'); ?></h1>
       </div>
    </div>
    </article>
</section>
<?php get_footer(); ?>
