<?php get_header(); ?>

    <?php require_once('includes/parts/main-page-slider.php'); ?>

    <section class="map">
        <h2 class="heading-2">Kelionės žemėlapyje</h2>
        <h3 class="heading-3">Susirask kelionės maršrutą akimirksniu</h3>
        <div class="map__holder">
            the map plugin goes here
        </div>
    </section>
    
    <?php require_once('includes/parts/main-page-trips.php'); ?>

    <?php require_once('includes/parts/main-page-top.php'); ?>     

    <?php require_once('includes/parts/main-page-tips.php'); ?>

<?php

// check if the repeater field has rows of data
if( have_rows('main_counters', 'option') ):
?>
<section class="info-icons">
    <div class="info-icons__wrapper">
<?php

 	// loop through the rows of data
    while ( have_rows('main_counters', 'option') ) : the_row();
    ?>
        <div class="info-icon" data-count="<?php the_sub_field('main_counters_count'); ?>"> 
            <figure class="info-icon__figure">
                <img src="<?php the_sub_field('main_counters_icon'); ?>" />
                <figcaption class="info-icon__caption">
                    <div class="info-icon__count"><?php the_sub_field('main_counters_count'); ?></div>
                    <h4 class="info-icon__title"><?php the_sub_field('main_counters_title'); ?></h4>
                </figcaption>
            </figure>
        </div>
    <?php endwhile; ?>
    </div>
</section>
<?php endif; ?>

    <section class="trips-instagram">
        <div class="trips-instagram__wrapper">
            <?php echo do_shortcode('[instagram-feed]'); ?>
        </div>
    </section>
<?php get_footer(); ?>
